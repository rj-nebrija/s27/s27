http = require('http')

let items = [

	{
		name : 'Iphone X',
		description : 'Phone designs and created by Apple',
		price : 30000,
	}, 
	{
		name : 'Horizon Forbidden West',
		description : 'Newest game for the PS4 and the PS5',
		price: 4000
	},
	{
		name : 'Razer Tiamat',
		description : 'Headset from Razer',
		price: 3000
	}
	
]

http.createServer((req, res) => {
	if(req.url === '/items' && req.method === 'GET') {
		let jsonItems = JSON.stringify(items)
		res.writeHead(200, {'Content-Type' : 'application/json'})
		res.end(jsonItems)
	}

	if(req.url === '/items' && req.method === 'POST') {

		let reqBody = ""
		req.on('data', (data)=> {
			reqBody += data
		})

		req.on('end', ()=> {

			let parsedItem = JSON.parse(reqBody)

			items.push(parsedItem)

			let stringifyItem = JSON.stringify(items)

			res.writeHead(200, {'Content-Type' : 'application/json'})
			res.end(stringifyItem)

			console.log("Sending back updated items " + stringifyItem)

		})
	}


	if(req.url === '/items/findItem' && req.method === 'POST') {
		let reqBody = ""

		req.on('data' , (data) => reqBody += data)

		req.on('end' , ()=> {
			console.log(reqBody)

			let parsedBody = JSON.parse(reqBody)

			var index = items.find((element)=> {
				return element.name === parsedBody.name
			})

			console.log(index)

			if(index) {
				let stringifyItem = JSON.stringify(index)

				res.writeHead(200, {'Content-Type' : 'application/json'})
				res.end(stringifyItem)
			} else {
				res.writeHead(400, {'Content-Type' : 'plain/text'})
				res.end('Item not found')
			}

		})
	}

}).listen(4000)


console.log('Server started on port 4000' )